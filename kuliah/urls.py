from django.contrib import admin
from django.urls import path
from .views import addClass, add, delete, college

urlpatterns = [
    path('addClass/', addClass),
    path('add/', add, name='add'),
    path('', college, name='college'),
    path('delete/P<int:delete_id>/',delete, name='delete'),

]
