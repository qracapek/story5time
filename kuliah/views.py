from django.http import HttpResponse
from django.shortcuts import render, redirect
from .forms import *
from .models import Matkul


def addClass(request):
    create_form = createForm()
    response = {'create_form':create_form}
    return render(request,"kuliah.html", response)

def add(request):
    if request.method == 'POST':
        form = createForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            matkul_input = Matkul()
            matkul_input.namamatkul = data['namamatkul']
            matkul_input.sks = data['sks']
            matkul_input.kelas = data['kelas']
            matkul_input.dosen = data['dosen']
            matkul_input.desc = data['desc']
            matkul_input.tahun = data['tahun']
            matkul_input.save()
            return redirect('/kuliah/')

def college(request):
    data = Matkul.objects.all()
    response = {'college':data}
    return render(request,'college.html',response)

def delete(request, delete_id):
    try:
        jadwal_to_delete = Matkul.objects.get(pk = delete_id)
        jadwal_to_delete.delete()
        return redirect('college')
    except:
        return redirect('college')
